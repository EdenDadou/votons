import React from 'react'
import {
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
} from 'react-native'
import Toast from 'react-native-toast-message'

const MAX_WIDTH = Dimensions.get('screen').width - 40

const BASE_VIEW = ({ type, text1, text2, onPress }) => {
  const MainComponent = typeof onPress === 'function' ? TouchableOpacity : View

  return (
    <MainComponent
      style={{
        ...styles.container,
      }}
    >
      {!!text1 && (
        <View style={styles.textContainer}>
          <Text
            color={type === 'error' ? '#fff' : 'black'}
            fontSize={15}
            align="center"
            style={styles.message}
          >
            {text1}
          </Text>
          <Text
            color={type === 'error' ? '#fff' : 'black'}
            fontSize={15}
            align="center"
            style={styles.message2}
          >
            {text2}
          </Text>
        </View>
      )}
    </MainComponent>
  )
}

const toastConfig = {
  success: BASE_VIEW,
  error: BASE_VIEW,
  info: BASE_VIEW,
}

export default function ToastConfig() {
  return <Toast config={toastConfig} />
}

const styles = StyleSheet.create({
  container: {
    margin: 20,
    borderRadius: 5,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowRadius: 20,
    shadowColor: '#b3b0b0',
    shadowOpacity: 0.5,
    elevation: 1,
    flexDirection: 'row',
    alignItems: 'center',
    maxWidth: MAX_WIDTH,
    borderWidth: 1,
    borderColor: 'blue',
    backgroundColor: '#f2f5fb',
  },
  textContainer: {
    padding: 16,
    alignSelf: 'center',
    flex: 1,
    borderWidth: 1,
    borderColor: 'red',
    margin: 3,
  },
  message: {
    alignSelf: 'center',
  },
  message2: {
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  action: {
    padding: 16,
    paddingLeft: 0,
  },
  actionText: {
    textTransform: 'uppercase',
  },
})
