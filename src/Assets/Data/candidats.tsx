/* eslint-disable no-sparse-arrays */
export const candidats = [
  {
    shortName: 'J-Luc Melenchon',
    fullName: 'Jean-Luc Melenchon',
    age: '70 ans',
    dateOfBirth: '19 août 1951',
    color: '#d3342f',
    partis: {
      name: 'La France Insoumise',
      img: require('../Images/partisPolitique/lfi.jpg'),
      history: [{ date: '2016', name: 'La France insoumise (Fondateur)' }],
    },
    network: {
      fb: 'https://fr-fr.facebook.com/JLMelenchon/',
      insta: 'https://www.instagram.com/jlmelenchon/?hl=fr',
      twitter:
        'https://twitter.com/JLMelenchon?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://melenchon2022.fr/',
      prog: 'https://melenchon2022.fr/programme/',
      actu: 'https://www.google.com/search?q=jean+luc+melenchon&source=lnms&tbm=nws&sa=X&ved=2ahUKEwjYttmMgvb0AhWLnRQKHQlbCwYQ_AUoAXoECAIQAw&biw=1792&bih=942&dpr=2',
      twitterURL:
        'https://twitter.com/search?q=%23Melenchon&src=typed_query&f=top',
    },
    picture: require('../Images/candidats/melenchon.jpg'),
    poste: {
      date: 'depuis 2017',
      titre: 'Député de la 4° circonscription des Bouches du Rhône (LFI)',
    },
    diplome: [
      {
        date: '1972',
        titre: 'Licences en Lettres modernes',
      },
      {
        date: '00/00/00',
        titre: 'Licence en Philosophie',
      },
    ],
    experience: [
      {
        date: '2017',
        titre: 'Député de la 4° circonscription des Bouches du Rhône (LFI)',
      },
      {
        date: '2009-2017',
        titre:
          'Député Européen (Gauche unitaire européenne/Gauche verte nordique)',
      },
      {
        date: '2008-2010',
        titre: 'CRC-SPG Groupe Communiste au Sénat',
      },
      {
        date: '2008-2016',
        titre: 'Parti de Gauche (Fondateur)',
      },
      {
        date: '2004-2008',
        titre: 'SOC Groupe Socialiste au Sénat',
      },
      {
        date: '2000-2002',
        titre:
          'Ministre Délégué à l’Enseignement professionnel (Président Jacques CHIRAC, Gouvernement Lionel JOSPIN)',
      },
      {
        date: '1998-2004',
        titre:
          'Président délégué du Conseil Général et Conseiller Général de l’Essonne',
      },
      {
        date: '1986-2000',
        titre: 'SOC GRoupe Socialiste au Sénat',
      },
      {
        date: '1986',
        titre: 'Sénateur',
      },
      {
        date: '1985',
        titre: "Conseiller général de l'Essonne",
      },
      {
        date: '1983',
        titre: 'Conseiller municipal de Massy',
      },
      {
        date: '1978',
        titre:
          'Directeur de cabinet de Claude Germon (Parti Socialiste), Mairie de Massy',
      },
      {
        date: '1977',
        titre: 'Dirige La tribune du Jura',
      },
      {
        date: '1976',
        titre: 'Journaliste & dessinateur La Voix du Jura',
      },
      {
        date: '1976-2006',
        titre: 'Membre du Parti Socialiste',
      },
      {
        date: '1975',
        titre: 'Professeur de Français dans un lycée technique',
      },
    ],
  },
  {
    fullName: 'Emmanuel Macron',
    age: '43 ans',
    dateOfBirth: '21 décembre 1977',
    color: '#045EDB',
    partis: {
      name: 'La Republique en Marche',
      img: require('../Images/partisPolitique/ENMARCHE.jpg'),
    },
    network: {
      fb: 'fb://profile?id=EmmanuelMacron',
      insta: 'https://www.instagram.com/emmanuelmacron/?hl=fr',
      twitter:
        'https://twitter.com/EmmanuelMacron?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://www.elysee.fr/emmanuel-macron',
      prog: 'https://en-marche.fr/emmanuel-macron/le-programme',
      actu: 'https://www.google.com/search?q=emmanuel+macron&source=lnms&tbm=nws&sa=X&ved=2ahUKEwiT1LOy-Ib1AhXuyYUKHb9ODWkQ_AUoAXoECAEQAw&biw=1792&bih=939&dpr=2',
      twitterURL:
        'https://twitter.com/search?q=%23EmmanuelMacron&src=typeahead_click',
    },
    picture: require('../Images/candidats/macron.jpg'),
    diplome: [
      {
        date: '2004',
        titre: 'Diplômé de l’ENA ',
      },
    ],
    poste: {
      date: 'depuis 2017',
      titre: 'Président de la République Française',
    },
    experience: [
      {
        date: 'depuis 2017',
        titre: 'Président de la République Française',
      },
      {
        date: '2016 – 2017',
        titre: 'Président d’En Marche',
      },
      {
        date: '2014-2016',
        titre:
          'Ministre de l’Economie, de l’Industrie et du Numérique (Président F. HOLLANDE, Gouvernement VALLS)',
      },
      {
        date: '2012-2014',
        titre:
          'Secrétaire général adjoint du cabinet du Président de la République (Président F. HOLLANDE)',
      },
      {
        date: '2010',
        titre: 'Associé gérant Rothschild & Cie',
      },
      {
        date: '2008',
        titre: 'Rejoint la banque d’affaires Rothschild & Cie',
      },
      {
        date: '2006',
        titre: 'Membre du Parti Socialiste',
      },
      {
        date: '2004',
        titre: 'Inspecteur des Finances',
      },
    ],
    programme: [
      {
        citation: 'depuis 2017',
        url: 'Président de la République Française',
        date: 'Président de la République Française',
        categorie: 'Président de la République Française',
      },
    ],
  },
  {
    fullName: 'Eric Zemmour',
    age: '63 ans',
    dateOfBirth: '31 août 1958',
    color: '#0B0B66',
    partis: {
      name: 'Reconquete',
      img: require('../Images/partisPolitique/reconquete.jpg'),
    },
    network: {
      fb: 'https://www.facebook.com/ZemmourEric/',
      insta: 'https://www.instagram.com/ericzemmour_/?hl=fr',
      twitter:
        'https://twitter.com/ZemmourEric?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://www.zemmour.fr/',
      prog: 'https://programme.zemmour2022.fr/',
      actu: 'https://www.google.com/search?q=eric+zemmour&source=lnms&tbm=nws&sa=X&ved=2ahUKEwiQ7-f0-4b1AhUMyYUKHXNiCOUQ_AUoAXoECAEQAw&biw=1792&bih=939&dpr=2',
      twitterURL:
        'https://twitter.com/search?q=%23Zemmour&src=typeahead_click&f=top',
    },
    picture: require('../Images/candidats/zemmour.jpg'),
    diplome: [
      {
        date: '1985',
        titre: 'Institut en Etudes Politiques (IEP) de Paris',
      },
    ],
    experience: [
      {
        date: '2019 à 2021',
        titre: 'Chroniqueur TV sur CNEWS',
      },
      {
        date: '2018',
        titre: 'Condamné pour provocation à la haine envers les musulmans',
      },
      {
        date: '2015',
        titre: 'Prix Combourg',
      },
      {
        date: '2013 à 2021',
        titre: 'Chroniqueur pour le Figaro Magazine',
      },
      {
        date: '2011',
        titre: 'Condamné pour provocation à la discrimination raciale',
      },
      {
        date: '2011',
        titre: 'Prix Richelieu',
      },
      {
        date: '2010 à 2016',
        titre: 'Chroniqueur Radio sur RTL',
      },
      {
        date: '2010',
        titre: 'Prix du livre incorrect ',
      },
      {
        date: '1996 à 2009',
        titre: 'Figaro (service politique)',
      },
      {
        date: '1986 à 1994',
        titre: 'Quotidien de Paris',
      },
      ,
    ],
  },
  {
    fullName: 'Yannick Jadot',
    age: '54 ans',
    dateOfBirth: '27 juillet 1967',
    partis: {
      name: 'Europe, écologie, les Verts',
      img: require('../Images/partisPolitique/eelv.jpg'),
      history: [{ date: '1999', name: 'Europe Ecologie-Les Verts' }],
    },
    color: '#29C254',
    picture: require('../Images/candidats/jadot.jpg'),
    network: {
      fb: 'https://www.facebook.com/yjadot/?fref=tag',
      insta: 'https://www.instagram.com/yannickjadot/?hl=fr',
      twitter:
        'https://twitter.com/yjadot?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://www.jadot2022.fr/',
      prog: 'https://www.eelv.fr/files/2021/10/Projet-2022-11.07.21-NP-1.pdf',
      actu: 'https://www.google.com/search?q=yannick+jadot&source=lnms&tbm=nws&sa=X&ved=2ahUKEwj5--aXhof1AhWDxIUKHWBwChwQ_AUoAXoECAEQAw&biw=1792&bih=939&dpr=2',
      twitterURL: 'https://twitter.com/search?q=%23Jadot&src=typed_query&f=top',
    },
    diplome: [
      {
        date: '00/00/00',
        titre: 'Maîtrise en commerce international (économie du développement)',
      },
    ],
    poste: { date: 'depuis 2009', titre: 'Député européen EELV' },
    experience: [
      {
        date: '2021',
        titre: "Remporte la primaire présidentielle de l'écologie",
      },
      {
        date: '2014',
        titre:
          "Tête de liste d'EELV (Circonscription Ouest) lors des élections européennes",
      },
      {
        date: '2010',
        titre:
          "Membre du bureau exécutif transitoire d'Europe-Écologie les Verts",
      },
      {
        date: '2009',
        titre:
          'Vice-président de la commission du commerce international, coordinateur pour les Verts et membre de la commission parlementaire (énergétiques, industrielles et de recherche)',
      },
      {
        date: '2008',
        titre: 'Quitte Greenpeace et rejoin les Verts',
      },
      {
        date: '2002-2008',
        titre: 'Directeur de campagne de Greenpeace France',
      },
      {
        date: '1995',
        titre:
          'Solagral ONG de solidarité internationale missions au Burkina Faso et au Bengladesh',
      },
    ],
  },
  {
    fullName: 'Marine Le Pen',
    age: '53 ans',
    dateOfBirth: '5 aout 1968',
    color: '#0B0B66',
    partis: {
      name: 'Rassemblement National',
      img: require('../Images/partisPolitique/rn.jpg'),
    },
    picture: require('../Images/candidats/lepen.jpg'),
    network: {
      fb: 'https://www.facebook.com/MarineLePen/',
      insta: 'https://www.instagram.com/marine_lepen/?hl=fr',
      twitter:
        'https://twitter.com/MLP_officiel?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://rassemblementnational.fr/',
      prog: 'https://rassemblementnational.fr/publications-thematiques/',
      actu: 'https://www.google.com/search?q=marine+le+pen&sxsrf=AOaemvKC1z8yBqq5flNg_En1wLv6x1kAlg:1640782538213&source=lnms&tbm=nws&sa=X&ved=2ahUKEwjW1N-ih4n1AhUIyxoKHVTmCSoQ_AUoAXoECAIQAw&biw=1629&bih=802&dpr=2.2',
      twitterURL: 'https://twitter.com/search?q=%23Lepen&src=typed_query',
    },
    diplome: [
      {
        date: '1991',
        titre: 'DEA de droit pénal',
      },
    ],
    poste: {
      date: 'depuis 07/2021',
      titre: 'Conseillère départementale et Députée du Pas-de-Calais',
    },
    experience: [
      {
        date: '2021',
        titre:
          "Quitte la tête du Rassemblement national et lance sa campagne en vue de l'élection présidentielle de 2022 (Nouveau président J BARDELA)",
      },
      {
        date: '2018',
        titre: 'Front national devient le « Rassemblement national »',
      },
      {
        date: '2017',
        titre: 'Second tour élection présidentielle contre Emmanuel Macron',
      },
      {
        date: '2016-2021',
        titre: 'Conseillère régionale des Hauts de France',
      },
      {
        date: '2011-2021',
        titre: 'Présidente du Front National puis Rassemblement National',
      },
      {
        date: '2004-2017',
        titre: 'Députée européenne (ENL)',
      },
      {
        date: '2004-2010',
        titre: 'Conseillère régionale d’Ile de France (FN)',
      },
      {
        date: '2000',
        titre: 'Siège au sein du bureau politique RN',
      },
      {
        date: '1998-2015',
        titre: 'Conseillère régionale du Nord-Pas-de-Calais (FN)',
      },
      {
        date: '1986',
        titre: 'Adhère au Front National',
      },
    ],
  },
  {
    fullName: 'Fabien Roussel',
    age: '52 ans',
    dateOfBirth: '16 avril 1969',
    color: '#d3342f',
    partis: {
      name: 'Parti communiste français',
      img: require('../Images/partisPolitique/pcf.jpg'),
    },
    picture: require('../Images/candidats/roussel.jpg'),
    network: {
      fb: 'https://www.facebook.com/FabienRoussel2022/',
      insta: 'https://www.instagram.com/fabien_roussel/?hl=fr',
      twitter:
        'https://twitter.com/Fabien_Roussel?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://www.fabienroussel2022.fr/donner?splash=1',
      prog: 'https://www.fabienroussel2022.fr/le_programme',
      actu: 'https://www.google.com/search?q=Fabien+Roussel&sxsrf=AOaemvLUBoO_UbtD2IzutbZR-4H8TqlDWg:1640784842315&source=lnms&tbm=nws&sa=X&ved=2ahUKEwj0x7btj4n1AhWR4IUKHRu0AY0Q_AUoAXoECAIQAw&biw=1629&bih=802&dpr=2.2',
      twitterURL:
        'https://twitter.com/search?q=%23Roussel&src=typed_query&f=top',
    },
    diplome: [
      {
        date: '1991',
        titre:
          'Centre de formation et de professionnalisation des journalistes',
      },
    ],
    poste: {
      date: 'depuis 2018',
      titre:
        'Secrétaire national du PCF & Député de la 20e circonscription du Nord',
    },
    experience: [
      {
        date: '2018-2021',
        titre: 'Secrétaire national du Parti Communiste',
      },
      {
        date: '2017-2021',
        titre: 'Député de la 20° circonscription du Nord depuis 2017',
      },
      {
        date: '2014-2021',
        titre: 'Conseiller municipal de Saint-Armand-les-Eaux',
      },
      {
        date: '2001-2001',
        titre:
          'Attaché parlementaire de Alain Bocquet (président du groupe communiste)',
      },
      {
        date: '1997-2001',
        titre:
          "Conseiller chargé de la communication pour Michelle Demessine, alors secrétaire d'État au Tourisme sous le gouvernement Jospin",
      },
    ],
  },
  {
    fullName: 'Philippe Poutou',
    age: '54 ans',
    dateOfBirth: '5 aout 1968',
    color: '#d3342f',
    partis: {
      name: 'Nouveau parti anticapitaliste',
      img: require('../Images/partisPolitique/npa.jpg'),
    },
    picture: require('../Images/candidats/poutou.jpg'),
    network: {
      fb: 'https://www.facebook.com/poutou.philippe/',
      insta: 'https://www.instagram.com/philippe.poutou/?hl=fr',
      twitter:
        'https://twitter.com/PhilippePoutou?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://nouveaupartianticapitaliste.org/',
      prog: 'https://nouveaupartianticapitaliste.org/',
      actu: 'https://www.google.com/search?q=Philippe+Poutou&biw=1629&bih=802&tbm=nws&sxsrf=AOaemvLr82vz96hjqNs_ve0fbkUPUWY1Lw%3A1640787425433&ei=4W3MYZb2GcSaa9jUq-AO&ved=0ahUKEwjW6pO9mYn1AhVEzRoKHVjqCuwQ4dUDCA0&uact=5&oq=Philippe+Poutou&gs_lcp=Cgxnd3Mtd2l6LW5ld3MQAzIICAAQgAQQsQMyCAgAEIAEELEDMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQ6BAgAEENQhwZY0QdgqQ5oAXAAeACAAUCIAXySAQEymAEAoAEBoAECsAEAwAEB&sclient=gws-wiz-news',
      twitterURL: 'https://twitter.com/search?q=%23Poutou&src=typed_query',
    },
    diplome: [
      {
        date: '1991',
        titre: 'Sans diplôme (Ouvrier)',
      },
    ],
    poste: {
      date: 'depuis 2020',
      titre: 'Conseiller municipal de Bordeaux',
    },
    experience: [
      {
        date: '2020-2021',
        titre:
          'Conseiller municipal de Bordeaux (depuis 2020) Mairie de Pierre HURMIC (EELV)',
      },
      {
        date: '2020-2021',
        titre: 'Conseiller métropolitain de Bordeaux Métropôle',
      },
      {
        date: '2014-2021',
        titre: 'Conseiller municipal de Saint-Armand-les-Eaux',
      },
      {
        date: '2008',
        titre:
          'La Voix des travailleurs devient le Nouveau Parti Anticapitaliste (NPA)',
      },
      {
        date: '2000',
        titre: 'Adherent La Voix des travailleurs ',
      },
      {
        date: '1985',
        titre: 'Adherent Lutte ouvrière',
      },
    ],
  },
  {
    fullName: 'Nathalie Arthaud',
    age: '51 ans',
    dateOfBirth: '5 aout 1968',
    color: '#d3342f',
    partis: {
      name: 'Luttes Ouvrière',
      img: require('../Images/partisPolitique/lutte-ouvriere.jpg'),
    },
    picture: require('../Images/candidats/arthaud.jpg'),
    network: {
      fb: 'https://www.facebook.com/nathaliearthaud/',
      insta: 'https://www.instagram.com/nathalie_arthaud_lo/',
      twitter:
        'https://twitter.com/n_arthaud?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://www.nathalie-arthaud.info/',
      prog: 'https://www.nathalie-arthaud.info/nos-positions',
      actu: 'https://www.google.com/search?q=nathalie+arthaud&sxsrf=AOaemvLaq13kShU1Nq7KuS5bp-rXd5MQLQ:1640787423982&source=lnms&tbm=nws&sa=X&ved=2ahUKEwjKoru8mYn1AhUxxIUKHb-mDOEQ_AUoAnoECAIQBA&biw=1629&bih=802&dpr=2.2',
      twitterURL: 'https://twitter.com/search?q=%23Arthaud&src=typed_query',
    },
    diplome: [
      {
        date: '00/00/00',
        titre: "CAPET & Agrégation d'économie et de gestion",
      },
    ],
    poste: {
      date: 'depuis 2008',
      titre:
        'Conseillère municipale de Vaulx-en-Velin & Porte-parole de Lutte ouvrière',
    },
    experience: [
      {
        date: '2017',
        titre: "Candidate Lutte ouvrière à l'élection présidentielle",
      },
      {
        date: '2008',
        titre: 'Porte-parole nationale Lutte ouvrière',
      },
      {
        date: '2008-2014',
        titre: 'Conseillère municipale chargée de la jeunesse à Vaulx-en-Velin',
      },
      {
        date: '2007',
        titre:
          "Porte-parole d'Arlette Laguiller lors de l'élection présidentielle",
      },
      {
        date: '1988',
        titre: 'Adhère à Lutte ouvrière',
      },
      {
        date: '1986',
        titre: 'Adhèrente Jeunesse communiste',
      },
    ],
  },
  {
    fullName: 'Anasse KAZIB',
    age: '34 ans',
    dateOfBirth: '1987',
    color: '#d3342f',
    partis: {
      name: 'Révolution permanente',
      img: require('../Images/partisPolitique/revolution-permanente.jpg'),
    },
    picture: require('../Images/candidats/kazib.jpg'),
    network: {
      fb: 'https://www.facebook.com/KazibAnasse/',
      insta: 'https://www.instagram.com/anassekazib/?hl=fr',
      twitter:
        'https://twitter.com/AnasseKazib?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://anassekazib2022.fr/',
      prog: 'https://anassekazib2022.fr/programme.php',
      actu: 'https://www.google.com/search?q=Anasse+Kazib&sxsrf=AOaemvKeygYUUgZy1NfcfMGzni8hxnG9zw:1640790430771&source=lnms&tbm=nws&sa=X&ved=2ahUKEwj2ipvWpIn1AhU6A2MBHS7DC_YQ_AUoAXoECAIQAw',
      twitterURL: 'https://twitter.com/search?q=%23Kazib&src=typed_query&f=top',
    },
    diplome: [
      {
        date: '00/00/00',
        titre: 'Sans diplômes (Cheminot)',
      },
    ],
    poste: {
      date: 'depuis 2012',
      titre: 'Cheminot & Délégué syndical SUD RAIL',
    },
    experience: [
      {
        date: '2019-2020',
        titre:
          'Participe au mouvement social contre la réforme des retraites (Gilets Jaunes)',
      },
      {
        date: '2018-2020',
        titre: 'Chroniqueurs aux Grandes Gueules sur RMC',
      },
      {
        date: '2015-2021',
        titre: 'Délégué syndical SUD RAIL',
      },
    ],
  },
  {
    fullName: 'Anne HIDALGO',
    age: '62 ans',
    dateOfBirth: '19 juin 1959',
    color: '#d3342f',
    partis: {
      name: 'Parti socialiste',
      img: require('../Images/partisPolitique/ps.jpg'),
    },
    picture: require('../Images/candidats/hidalgo.jpg'),
    network: {
      fb: 'https://www.facebook.com/HidalgoAnne/',
      insta: 'https://www.instagram.com/annehidalgo/?hl=fr',
      twitter:
        'https://twitter.com/Anne_Hidalgo?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://www.2022avechidalgo.fr/',
      prog: 'https://www.2022avechidalgo.fr/nos_combats',
      actu: 'https://www.google.com/search?q=anne+hidalgo&biw=1629&bih=802&tbm=nws&sxsrf=AOaemvJf2ZDJrMrl55wsBt4WCySf2i2uwg%3A1640791089354&ei=MXzMYdWLFcrClwTKgpGgCA&ved=0ahUKEwiV6J-Qp4n1AhVK4YUKHUpBBIQQ4dUDCA0&uact=5&oq=anne+hidalgo&gs_lcp=Cgxnd3Mtd2l6LW5ld3MQAzIICAAQgAQQsQMyCAgAEIAEELEDMggIABCABBCxAzILCAAQgAQQsQMQgwEyBwgAELEDEEMyBQgAELEDMggIABCxAxCDATIECAAQQzIFCAAQgAQyBQgAEIAEUKgEWOwHYK0JaABwAHgAgAFNiAHUAZIBATOYAQCgAQHAAQE&sclient=gws-wiz-news',
      twitterURL:
        'https://twitter.com/search?q=%23Hidalgo&src=typeahead_click&f=top',
    },
    diplome: [
      {
        date: '00/00/00',
        titre: 'Maîtrise en Sciences sociales du travail',
      },
      {
        date: '00/00/00',
        titre: 'DEA en Droit social et syndical',
      },
    ],
    poste: {
      date: 'depuis 2012',
      titre:
        'Maire de Paris & Première vice-présidente de la Métropole du Grand Paris',
    },
    experience: [
      {
        date: '2020-2021',
        titre: 'Conseillère du 11° arr de Paris (socialiste)',
      },
      {
        date: '2016-2021',
        titre: 'Conseillère métropolitaine du Grand Paris',
      },
      {
        date: '2016-2021',
        titre: 'Première vice-présidente de la Métropole du Grand Paris',
      },
      {
        date: '2014-2021',
        titre: 'Maire de Paris (PS-PCF-EELV)',
      },
      {
        date: '2004-2014',
        titre: "Conseillère régionale d'Île-de-France",
      },
      {
        date: '2001-2020',
        titre: 'Conseillère du 15° arr de Paris (socialiste)',
      },
      {
        date: '2001-2014',
        titre: 'Première adjointe au maire de Paris (Bertrand Delanoë)',
      },
      {
        date: '1997-2002',
        titre:
          'Missions dans différents cabinets ministériels du gouvernement JOSPIN',
      },
      {
        date: '1994',
        titre: 'Adhère au Parti Socialiste',
      },
      {
        date: '1984-2011',
        titre: 'Inspectrice du travail',
      },
    ],
  },
  {
    fullName: 'Pierre LARROUTUROU',
    shortName: 'P. LARROUTUROU',
    age: '57 ans',
    dateOfBirth: '19 octobre 1964',
    color: '#d3342f',
    partis: {
      name: 'Nouvelle donne',
      img: require('../Images/partisPolitique/nouvelle-donne.jpg'),
    },
    picture: require('../Images/candidats/larroutrou.jpg'),
    network: {
      fb: 'https://www.facebook.com/larrouturou.pierre/',
      insta: 'https://www.instagram.com/pierre.larrouturou/?hl=fr',
      twitter:
        'https://twitter.com/larrouturou?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'http://www.pierre-larrouturou.eu/',
      prog: 'https://www.nouvelledonne.fr/notre-programme/',
      actu: 'https://www.google.com/search?q=Pierre+LARROUTUROU&sxsrf=AOaemvLOCxvVZ0Izcj1zCdIUK21FTU9R7w:1640791980085&source=lnms&tbm=nws&sa=X&ved=2ahUKEwiO4P24qon1AhW1DmMBHQITDfUQ_AUoAXoECAIQAw&cshid=1640792195330575&biw=1629&bih=802&dpr=2.2',
      twitterURL:
        'https://twitter.com/search?q=%23Larrouturou&src=typeahead_click&f=top',
    },
    diplome: [
      {
        date: '00/00/00',
        titre: 'Ingénieur agronome',
      },
      {
        date: '00/00/00',
        titre: 'Diplômé de lnstitut d’études politiques (IEP)',
      },
    ],
    poste: {
      date: 'depuis 2017',
      titre: 'Porte Parole de Nouvelle Donne & Député Européen',
    },
    experience: [
      {
        date: '2019-2021',
        titre:
          'Député Européen S&D (Alliance progressiste des socialistes et démocrates au parlement européen)',
      },
      {
        date: '2017-2021',
        titre: 'Porte Parole de Nouvelle Donne',
      },
      {
        date: '2013-2016',
        titre: 'Co-président de Nouvelle Donne',
      },
      {
        date: '2013',
        titre: 'Quitte le PS et crée le partis Nouvelle Donne',
      },
      {
        date: '2010-2015',
        titre: "Conseiller régionale d'Île-de-France",
      },
      {
        date: '2011',
        titre: 'Quitte EELV et rejoin le PS',
      },
      {
        date: '2009',
        titre: 'Quitte le PS et rejoin EELV',
      },
      {
        date: '1994',
        titre:
          "Crée le Comité d'action pour le passage rapide aux 4 jours sur 5 (Cap4J/5)",
      },
      {
        date: '1988',
        titre: 'Adhère au parti socialiste',
      },
    ],
  },
  {
    fullName: 'Arnaud MONTEBOURG',
    shortName: 'A. MONTEBOURG',
    age: '59 ans',
    dateOfBirth: '30 octobre 1962',
    color: '#d3342f',
    partis: {
      name: 'La Remontada de la france',
      // img: require('../Images/partisPolitique/nouvelle-donne.jpg'),
    },
    picture: require('../Images/candidats/montebourg.jpg'),
    network: {
      fb: 'https://www.facebook.com/arnaudmontebourg.fr',
      insta: 'https://www.instagram.com/arnaud_montebourg/?hl=fr',
      twitter:
        'https://twitter.com/montebourg?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://laremontada.fr/',
      prog: 'https://laremontada.fr/#projet',
      actu: 'https://www.google.com/search?q=arnaud+MONTEBOURG&sxsrf=AOaemvJ3Ck5lgKIG6eBUHY636cvfMBBquA:1640796855288&source=lnms&tbm=nws&sa=X&ved=2ahUKEwjJttTNvIn1AhUOFRQKHUtbCPUQ_AUoAXoECAIQAw&biw=1629&bih=802&dpr=2.2',
      twitterURL:
        'https://twitter.com/search?q=%23Montebourg&src=typed_query&f=top',
    },
    diplome: [
      {
        date: '00/00/00',
        titre: 'Licence de Droit Université Paris 1 Sorbonne',
      },
      {
        date: '00/00/00',
        titre: 'Diplômé de lnstitut d’études politiques (IEP)',
      },
    ],
    poste: {
      date: 'depuis 2017',
      titre: 'Entrepreneur agroalimentaire (miel, amandes & lait)',
    },
    experience: [
      {
        date: '2012-2014',
        titre:
          'Ministre de l’Economie, du Redressement productif et du Numérique (Président HOLLANDE)',
      },
      {
        date: '2008-2015',
        titre:
          'Conseiller général de Saône et Loire, élu dans le canton de Montret.',
      },
      {
        date: '2008-2012',
        titre: 'Président du Conseil Général de Saône et Loire',
      },
      {
        date: '2008-2012',
        titre:
          'Secrétaire national du Parti socialiste, chargé de la rénovation',
      },
      {
        date: '2007',
        titre: 'Porte-parole de Ségolène Royal pour la présidentielle',
      },
      {
        date: '2003-2012',
        titre:
          'Membre du bureau national et du conseil national du Parti socialiste.',
      },
      {
        date: '1997-2012',
        titre: 'Député de la 6° circonscription de Saône et Loire',
      },
      {
        date: '1992',
        titre:
          'Premier secrétaire de la conférence du stage des avocats de Paris',
      },
      {
        date: '1990',
        titre: "Début de carrière d'avocat à la cour d’appel de Paris",
      },
      {
        date: '1985',
        titre: 'Militant au Parti socialiste',
      },
    ],
  },
  {
    fullName: 'Valérie PECRESSE',
    age: '54 ans',
    dateOfBirth: '14 juillet 1967',
    color: '#045EDB',
    partis: {
      name: 'Les Républicains',
      img: require('../Images/partisPolitique/lr.jpg'),
    },
    picture: require('../Images/candidats/pecresse.jpg'),
    network: {
      fb: 'https://www.facebook.com/vpecresse',
      insta: 'https://www.instagram.com/vpecresse/?hl=fr',
      twitter:
        'https://twitter.com/vpecresse?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://www.valeriepecresse.fr/',
      prog: 'https://www.valeriepecresse.fr/mon-projet/',
      actu: 'https://www.google.com/search?q=valerie+pecresse&sxsrf=AOaemvLMVYjZGRYARS5U2qJWOcLmYtaiJw:1640807093383&source=lnms&tbm=nws&sa=X&ved=2ahUKEwimmsjf4on1AhVQXRoKHUuACB8Q_AUoAXoECAIQAw&biw=1629&bih=802&dpr=2.2',
      twitterURL:
        'https://twitter.com/search?q=%23Pecresse&src=typed_query&f=top',
    },
    diplome: [
      {
        date: '1988',
        titre: 'École des hautes études commerciales (HEC)',
      },
      {
        date: '1992',
        titre: 'École Nationale d’Administration (ENA)',
      },
    ],
    poste: {
      date: 'depuis 2017',
      titre: 'Présidente de Soyons libres',
    },
    experience: [
      {
        date: '2020-2021',
        titre: 'Conseillère municipale de Vélizy Villacoublay (IDF)',
      },
      {
        date: '2020-2021',
        titre: 'Conseillère communautaire du Versailles Grand Parc',
      },
      {
        date: '2015-2021',
        titre: 'Présidente du Conseil Regional d’Ile de France',
      },
      {
        date: '2012-2016',
        titre: 'Députée de la 2° circonscription des Yvelines (UMP puis LR)',
      },
      {
        date: '2011-2012',
        titre:
          'Ministre du Budget, des Comptes publics et de la Réforme de l’Etat',
      },
      {
        date: '2011-2012',
        titre: 'Porte Parole du gouvernement (Président N. SARKOZY)',
      },
      {
        date: '2007-2011',
        titre:
          'Ministre de l’Enseignement supérieur et de la Recherche (Président N. SARKOZY)',
      },
      {
        date: '2004-2021',
        titre: 'Conseillère Régionale d’Ile de France',
      },
      {
        date: '2002-2007',
        titre: 'Députée de la 2° circonscription des Yvelines (UMP)',
      },
      {
        date: '1992-2015',
        titre: 'Maître des requêtes au Conseil d’Etat',
      },
      {
        date: '1992-1998',
        titre: 'Enseignante à l’IEP de Paris',
      },
    ],
  },
  {
    fullName: 'Nicolas DUPONT-AIGNAN',
    shortName: 'N. DUPONT-AIGNAN',
    age: '60 ans',
    dateOfBirth: '7 mars 1961',
    color: '#045EDB',
    partis: {
      name: 'Debout la France',
      img: require('../Images/partisPolitique/debout-la-france.jpg'),
    },
    picture: require('../Images/candidats/dupont-aignan.jpg'),
    network: {
      fb: 'https://www.facebook.com/nicolasdupontaignan/',
      insta: 'https://www.instagram.com/dupontaignan/?hl=fr',
      twitter:
        'https://twitter.com/dupontaignan?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://www.debout-la-france.fr/',
      prog: 'https://2022nda.fr/projet/',
      actu: 'https://www.google.com/search?q=nicolas+dupont+aignan&sxsrf=AOaemvIDwOpyXES4ZlegFF3gwFfAJcj9nw:1640810044498&source=lnms&tbm=nws&sa=X&ved=2ahUKEwjj--He7Yn1AhUq8uAKHWVJBvUQ_AUoAXoECAIQAw&biw=1629&bih=802&dpr=2.2',
      twitterURL:
        'https://twitter.com/search?q=%23Pecresse&src=typed_query&f=top',
    },
    diplome: [
      {
        date: '00/00/00',
        titre: 'Licence en Droit',
      },
      {
        date: '00/00/00',
        titre: 'Institut d’études politiques (IEP)',
      },
      {
        date: '00/00/00',
        titre: "École National d'Administration (ENA)",
      },
      {
        date: '00/00/00',
        titre:
          'Études supérieures spécialisées en gestion de l’entreprise et marché financier',
      },
    ],
    poste: {
      date: 'depuis 2017',
      titre: 'Président de Debout la France',
    },
    experience: [
      {
        date: '2014',
        titre: 'Debout la République renommé Debout la France',
      },
      {
        date: '2008',
        titre: 'Debout la République devient un parti',
      },
      {
        date: '2008',
        titre: 'Président de Debout La France',
      },
      {
        date: '2002-2017',
        titre: 'Président de la communauté d’agglomération du Val d’Yerres',
      },
      {
        date: '2002',
        titre: "Adhère à l'UMP",
      },
      {
        date: '1999-2000',
        titre:
          'Secrétaire genéral adjoint du Rassemblement pour la France (Charles PASQUA)',
      },
      {
        date: '1999',
        titre: 'Fonde Debout La République (mouvement au sein du RPR)',
      },
      {
        date: '1997-2007',
        titre: 'Député de la 8° circonscription de l’Essonne depuis 1997',
      },
      {
        date: '1995-2017',
        titre: 'Maire d’Yerres',
      },
      {
        date: '1994',
        titre:
          'Conseiller technique du ministère de l’éducation nationale F. BAYROU, puis conseiller technique du cabinet du ministre de l’environnement M. BARNIER',
      },
      {
        date: '1993',
        titre:
          'Directeur adjoint du cabinet du préfet de la région Ile de France, puis chef de cabinet',
      },
    ],
  },
  {
    fullName: 'Jean LASSALE',
    age: '66 ans',
    dateOfBirth: '7 mars 1955',
    color: '#045EDB',
    partis: {
      name: 'Résistons !',
      img: require('../Images/partisPolitique/resistons.jpg'),
    },
    picture: require('../Images/candidats/lassalle.jpg'),
    network: {
      fb: 'https://www.facebook.com/lassalle.jean/',
      insta: 'https://www.instagram.com/jeanlassalleoff/?hl=fr',
      twitter:
        'https://twitter.com/jeanlassalle?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor',
      site: 'https://resistons-france.fr/',
      prog: 'https://resistons-france.fr/projet/',
      actu: 'https://www.google.com/search?q=jean+lassalle&sxsrf=AOaemvJChqQZq90yc3SZMnTV3sjLMuE8Rw:1640815496323&source=lnms&tbm=nws&sa=X&ved=2ahUKEwj68LKGgor1AhVZA2MBHXFEDvQQ_AUoA3oECAIQBQ&biw=1629&bih=802&dpr=2.2',
      twitterURL:
        'https://twitter.com/search?q=%23Lassale&src=typed_query&f=top',
    },
    diplome: [
      {
        date: '00/00/00',
        titre:
          'Technicien agricole spécialisé dans l’hydrolique et l’aménagement du territoire',
      },
    ],
    poste: {
      date: 'depuis 2017',
      titre: 'Président de Résistons',
    },
    experience: [
      {
        date: '2020-2021',
        titre: 'Liberté et Territoires (Groupe parlementaire centriste)',
      },
      {
        date: '2016',
        titre: 'Président de Résistons (fondateur)',
      },
      {
        date: '2010-2016',
        titre: 'Vice président du mouvement Démocrate (MODEM)',
      },
      {
        date: '2002-2021',
        titre: 'Député de la 4° circonscription des Pyrénées Atlantiques',
      },
      {
        date: '2002–2007',
        titre: "Adhère à l'UDF",
      },
      {
        date: '1994-2011',
        titre: 'Président de l’institution patrimoniale du Haut Béarn',
      },
      {
        date: '1991-2001',
        titre: 'Vice Président du Conseil Général des Pyrénées atlantiques',
      },
      {
        date: '1989-1999',
        titre: 'Président du Parc National de Pyrénées',
      },
      {
        date: '1982-2015',
        titre: 'Conseiller général des Pyrénées Atlantiques',
      },
      {
        date: '1977-2017',
        titre: 'Maire de Lourdios-Ichère',
      },
    ],
  },
]
