import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { IPropsItem } from '@/Types/interface'
import { Image, Text, View, Dimensions, StyleSheet } from 'react-native'
import { useTheme } from '@/Hooks'
import { navigate } from '@/Navigators/utils'
const width = Dimensions.get('window').width

const ItemCandidats = ({ item }: IPropsItem) => {
  const { Fonts, Gutters, Layout } = useTheme()

  return (
    <TouchableOpacity
      onPress={() => navigate('Detail', { item: item })}
      style={[Layout.colCenter, Gutters.smallVMargin, { width: width / 2 }]}
    >
      <View>
        <Image
          style={[Layout.fullSize, styles.flatListImg]}
          source={item.picture}
          resizeMode="cover"
        />
        {item.partis.img && (
          <Image
            style={[
              styles.partisImg,
              {
                borderColor: item.color || 'white',
              },
            ]}
            source={item.partis.img}
            resizeMode="cover"
          />
        )}
      </View>
      <View style={[Layout.fullWidth, Layout.colCenter]}>
        <Text style={[Fonts.textRegular, styles.textCdt]}>
          {item?.shortName ? item.shortName : item.fullName}
        </Text>
        <Text style={[Fonts.textSmall]}>{item.partis.name}</Text>
      </View>
    </TouchableOpacity>
  )
}

export default ItemCandidats

const styles = StyleSheet.create({
  partisImg: {
    borderRadius: 17.5,
    height: 35,
    width: 35,
    position: 'absolute',
    bottom: -2,
    right: -3,
    borderWidth: 2,
  },
  flatListImg: { borderRadius: 60, height: 120, width: 120 },
  textCdt: { fontWeight: 'bold', marginTop: 3 },
})
