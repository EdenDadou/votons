/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react'
import { FlatList } from 'react-native-gesture-handler'
import { candidats } from '@/Assets/Data/candidats'
import ItemCandidats from './components/itemCandidats'
import { View, StyleSheet } from 'react-native'
import { useTheme } from '@/Hooks'
import { IPropsItem } from '@/Types/interface'
import Toast from 'react-native-toast-message'
import intervalToDuration from 'date-fns/intervalToDuration'

const Home = () => {
  const { Layout } = useTheme()

  const renderItem = ({ item }: IPropsItem) => <ItemCandidats item={item} />

  const now = new Date()
  const first = new Date('2022/04/10')
  const timeToFirst = intervalToDuration({ start: now, end: first })

  useEffect(() => {
    Toast.show({
      type: 'success',
      text1: "Premier Tour de l'élection dans",
      text2: `${timeToFirst.months} mois, ${timeToFirst.days} jours et ${timeToFirst.hours} heure.`,
    })
  }, [])

  function SortArray(x: any, y: any) {
    if (x.fullName.split(' ')[1] < y.fullName.split(' ')[1]) {
      return -1
    }
    if (x.fullName.split(' ')[1] > y.fullName.split(' ')[1]) {
      return 1
    }
    return 0
  }

  return (
    <View style={[Layout.fullSize]}>
      <FlatList
        contentContainerStyle={[styles.flatListContainer]}
        horizontal={false}
        numColumns={2}
        data={candidats.sort(SortArray)}
        renderItem={renderItem}
        keyExtractor={item => item.fullName}
      />
      <Toast />
    </View>
  )
}

export default Home

const styles = StyleSheet.create({
  icon: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  flatListContainer: {
    width: '100%',
    justifyContent: 'space-around',
    paddingTop: 20,
  },
})
