import React, { useState } from 'react'
import { IPropsCandidats } from '@/Types/interface'
import { useTheme } from '@/Hooks'
import Candidat from './components/candidat'
import Network from './components/network'
import Info from './components/info'
import { ScrollView } from 'react-native-gesture-handler'

const Detail = ({ route }: IPropsCandidats) => {
  const { Gutters, Layout } = useTheme()
  let { item } = route.params
  const [infoOpen, setOpenInfo] = useState(0)
  const [dataSourceCords, setDataSourceCords] = useState<any>({
    actu: 0,
    exp: 0,
    prog: 0,
    twitter: 0,
  })
  const [ref, setRef] = useState<any>(null)

  function handleInfoOpen(idInfo: number, topic: string) {
    if (idInfo !== infoOpen) {
      setTimeout(() => {
        ref.scrollTo({
          x: 0,
          y: dataSourceCords[topic] + 305,
          animated: true,
        })
      }, 100)
    }
    infoOpen === idInfo ? setOpenInfo(0) : setOpenInfo(idInfo)
  }

  return (
    <ScrollView
      ref={el => {
        setRef(el)
      }}
      contentContainerStyle={[Layout.colCenter, Gutters.smallVMargin]}
    >
      <Candidat item={item} />
      <Network item={item} />
      <Info
        item={item}
        infoOpen={infoOpen}
        handleInfoOpen={handleInfoOpen}
        setDataSourceCords={setDataSourceCords}
        dataSourceCords={dataSourceCords}
      />
    </ScrollView>
  )
}

export default Detail
