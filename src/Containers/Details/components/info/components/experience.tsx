import { IExperience, IPropsItem } from '@/Types/interface'
import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const Experience = ({ item }: IPropsItem) => {
  return (
    <View style={[styles.body]}>
      <View
        style={[
          styles.friseVertical,
          {
            backgroundColor: item.color,
          },
        ]}
      />
      {item.experience.map((each: IExperience) => (
        <View style={styles.expContainer} key={each.titre}>
          <View
            style={[
              styles.expPuce,
              {
                backgroundColor: item.color,
              },
            ]}
          >
            <Text style={styles.txtDate}>{`${each.date}`}</Text>
          </View>
          <Text style={styles.txtExp}>{`${each.titre}`}</Text>
        </View>
      ))}
    </View>
  )
}

export default Experience

const styles = StyleSheet.create({
  body: { width: '100%', marginBottom: 20 },
  expContainer: {
    marginHorizontal: '2%',
    flexDirection: 'row',
    marginBottom: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  expPuce: {
    height: 30,
    width: 80,
    borderRadius: 20,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  friseVertical: {
    width: 8,
    height: '100%',
    position: 'absolute',
    top: -30,
    left: 45,
  },
  txtExp: {
    width: '70%',
    marginRight: '5%',
    flexDirection: 'row',
    color: 'red',
  },
  txtDate: {
    fontWeight: '800',
    fontSize: 11,
  },
})
