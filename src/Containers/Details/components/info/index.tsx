import { IPropsInfo } from '@/Types/interface'
import React, { useState } from 'react'
import { View, Text, Image, StyleSheet, ActivityIndicator } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Experience from './components/experience'
import WebView from 'react-native-webview'

const Info = ({
  item,
  infoOpen,
  handleInfoOpen,
  dataSourceCords,
  setDataSourceCords,
}: IPropsInfo) => {
  const [webviewMargin, setWebviewMargin] = useState('-45%')
  return (
    <View style={styles.body}>
      {/* //********** Experince ****************** */}
      <View style={styles.hideBackgroundWebview}>
        <View
          key={'exp'}
          onLayout={event => {
            const layout = event.nativeEvent.layout
            dataSourceCords.exp = layout.y
            setDataSourceCords(dataSourceCords)
          }}
        >
          <TouchableOpacity
            style={[
              styles.buttonInfo,
              {
                backgroundColor: item.color,
              },
            ]}
            onPress={() => handleInfoOpen(1, 'exp')}
          >
            <Text style={styles.textInfo}>Dates clés</Text>
            <Image
              style={styles.chevronSize}
              source={
                infoOpen === 1
                  ? require('@/Assets/Images/iconApp/down.png')
                  : require('@/Assets/Images/iconApp/up.png')
              }
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>

        {infoOpen === 1 && <Experience item={item} />}
        {/* //********** Programme ****************** */}
        <View
          key={'prog'}
          onLayout={event => {
            const layout = event.nativeEvent.layout
            dataSourceCords.prog = layout.y
            setDataSourceCords(dataSourceCords)
          }}
        >
          <TouchableOpacity
            style={[
              styles.buttonInfo,
              {
                backgroundColor: item.color,
              },
            ]}
            onPress={() => handleInfoOpen(2, 'prog')}
          >
            <Text style={styles.textInfo}>Positions</Text>
            <Image
              style={styles.chevronSize}
              source={
                infoOpen === 2
                  ? require('@/Assets/Images/iconApp/down.png')
                  : require('@/Assets/Images/iconApp/up.png')
              }
              resizeMode="contain"
            />
          </TouchableOpacity>
          {infoOpen === 2 && <Experience item={item} />}
        </View>
      </View>
      {/* //********** Actu ****************** */}
      <View
        key={'actu'}
        onLayout={event => {
          const layout = event.nativeEvent.layout
          dataSourceCords.actu = layout.y
          setDataSourceCords(dataSourceCords)
        }}
      >
        <TouchableOpacity
          style={[
            styles.buttonInfo,
            {
              backgroundColor: item.color,
            },
          ]}
          onPress={() => handleInfoOpen(3, 'actu')}
        >
          <Text style={styles.textInfo}>Actu</Text>
          <Image
            style={styles.chevronSize}
            source={
              infoOpen === 3
                ? require('@/Assets/Images/iconApp/down.png')
                : require('@/Assets/Images/iconApp/up.png')
            }
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
      {infoOpen === 3 && (
        <View
          style={[
            styles.webViewContainer,
            {
              marginTop: webviewMargin,
            },
          ]}
        >
          <WebView
            source={{ uri: item.network.actu }}
            javaScriptEnabled
            domStorageEnabled
            sharedCookiesEnabled
            contentMode={'mobile'}
            onNavigationStateChange={e => {
              console.log(e)
              if (e.title.includes('Google') || e.loading) {
                setWebviewMargin('-45%')
              } else {
                setWebviewMargin('0%')
              }
            }}
            renderLoading={() => <ActivityIndicator />}
          />
        </View>
      )}
      {/* //********** TWitter ****************** */}
      <View
        key={'twitter'}
        onLayout={event => {
          const layout = event.nativeEvent.layout
          dataSourceCords.twitter = layout.y
          setDataSourceCords(dataSourceCords)
        }}
      >
        <TouchableOpacity
          style={[
            styles.buttonInfo,
            {
              backgroundColor: item.color,
            },
          ]}
          onPress={() => handleInfoOpen(4, 'twitter')}
        >
          <Text style={styles.textInfo}>Twitter</Text>
          <Image
            style={styles.chevronSize}
            source={
              infoOpen === 4
                ? require('@/Assets/Images/iconApp/down.png')
                : require('@/Assets/Images/iconApp/up.png')
            }
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>

      {infoOpen === 4 && (
        <View style={[styles.webViewContainer]}>
          <WebView
            source={{
              uri: item.network.twitterURL,
            }}
            javaScriptEnabled
            domStorageEnabled
            sharedCookiesEnabled
            contentMode={'mobile'}
            renderLoading={() => <ActivityIndicator />}
          />
        </View>
      )}
    </View>
  )
}

export default Info

const styles = StyleSheet.create({
  body: { flex: 1, width: '100%', flexGrow: 1 },
  buttonInfo: {
    height: 60,
    paddingLeft: 50,
    paddingRight: 20,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
    backgroundColor: 'white',
  },
  chevronSize: { width: 15, height: 15 },
  textInfo: { fontSize: 22, fontWeight: '700' },
  webViewContainer: {
    height: 800,
    width: '100%',
    zIndex: -10,
  },
  hideBackgroundWebview: { backgroundColor: 'inherit' },
})
