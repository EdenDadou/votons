import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import { IPropsItem } from '@/Types/interface'
import { useTheme } from '@/Hooks'

const Candidat = ({ item }: IPropsItem) => {
  const { Fonts } = useTheme()
  return (
    <View style={[styles.headContainer]}>
      <View>
        <Image style={styles.cdtImg} source={item.picture} resizeMode="cover" />

        {item?.partis?.img && (
          <Image
            style={[
              styles.partisImg,
              {
                borderColor: item.color || 'white',
              },
            ]}
            source={item.partis.img}
            resizeMode="cover"
          />
        )}
      </View>
      <View style={styles.infoContainer}>
        <View style={{ marginBottom: 5 }}>
          <Text style={styles.txtName}>{item.fullName}</Text>
          <Text style={[styles.txtBirth, Fonts.textRegular2]}>
            {`${item.age} (${item.dateOfBirth})`}
          </Text>
        </View>

        {item.diplome && (
          <View style={styles.subInfoContainer}>
            {item.diplome.map((each, index) => (
              <Text
                key={index}
                style={styles.txtDiplome}
              >{` ${each.titre}`}</Text>
            ))}
          </View>
        )}

        <Text style={styles.txtPoste}>{item?.poste?.titre}</Text>
        <Text style={[styles.txtPartis, Fonts.textRegular2]}>
          {item.partis.name}
        </Text>
      </View>
    </View>
  )
}

export default Candidat

const styles = StyleSheet.create({
  headContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 200,
    paddingHorizontal: 15,
    marginBottom: 25,
  },
  cdtImg: { borderRadius: 90, height: 200, width: 140 },
  partisImg: {
    borderRadius: 20,
    height: 40,
    width: 40,
    position: 'absolute',
    bottom: 0,
    right: -3,
    borderWidth: 2,
  },
  txtName: {
    textAlign: 'right',
    fontWeight: 'bold',
    fontSize: 18,
  },
  txtBirth: {
    fontSize: 15,
    textAlign: 'right',
    marginBottom: 10,
  },
  txtPartis: {
    fontSize: 18,
    fontWeight: '600',
    textAlign: 'right',
    textAlignVertical: 'bottom',
  },
  infoContainer: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 10,
    // backgroundColor: 'green',
  },
  subInfoContainer: {
    marginBottom: 22,
  },
  txtDiplome: {
    fontSize: 11,
    textAlign: 'right',
  },
  txtPoste: {
    fontSize: 13,
    fontWeight: 'bold',
    textAlign: 'right',
    marginBottom: 15,
  },
})
