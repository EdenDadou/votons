import React from 'react'
import { View, Image, StyleSheet, Dimensions, Linking } from 'react-native'
import { IPropsItem } from '@/Types/interface'
import { TouchableOpacity } from 'react-native-gesture-handler'
const { width } = Dimensions.get('window')
const ICON_SIZE = (width * 0.8) / 5

const Network = ({ item }: IPropsItem) => {
  async function OpenURL(url: string) {
    setTimeout(function () {
      Linking.openURL(
        url.replace('fb://profile?id=', 'https://fr-fr.facebook.com/'),
      )
    }, 25)
    Linking.openURL(url)
  }

  return (
    <View style={styles.iconsContainer}>
      <TouchableOpacity
        style={[
          styles.iconContainer,
          {
            backgroundColor: item.color,
          },
        ]}
        onPress={() => OpenURL(item?.network?.fb)}
      >
        <Image
          style={{ width: ICON_SIZE - 20, height: ICON_SIZE - 20 }}
          source={require('@/Assets/Images/logoSocialNetwork/facebook.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={[
          styles.iconContainer,
          {
            backgroundColor: item.color,
          },
        ]}
        onPress={() => Linking.openURL(item?.network?.insta)}
      >
        <Image
          style={{ width: ICON_SIZE - 20, height: ICON_SIZE - 20 }}
          source={require('@/Assets/Images/logoSocialNetwork/insta.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={[
          styles.iconContainer,
          {
            backgroundColor: item.color,
          },
        ]}
        onPress={() => Linking.openURL(item?.network?.twitter)}
      >
        <Image
          style={{ width: ICON_SIZE - 20, height: ICON_SIZE - 20 }}
          source={require('@/Assets/Images/logoSocialNetwork/twitter.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={[
          styles.iconContainer,
          {
            backgroundColor: item.color,
          },
        ]}
        onPress={() => Linking.openURL(item.network.site)}
      >
        <Image
          style={{ width: ICON_SIZE - 20, height: ICON_SIZE - 20 }}
          source={require('@/Assets/Images/logoSocialNetwork/cursor.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={[
          styles.iconContainer,
          {
            backgroundColor: item.color,
          },
        ]}
        onPress={() => Linking.openURL(item?.network?.prog)}
      >
        <Image
          style={{ width: ICON_SIZE - 20, height: ICON_SIZE - 20 }}
          source={require('@/Assets/Images/logoSocialNetwork/info.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </View>
  )
}

export default Network

const styles = StyleSheet.create({
  iconsContainer: {
    width: '100%',
    height: 60,
    paddingHorizontal: '5%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  iconContainer: {
    width: ICON_SIZE,
    height: ICON_SIZE,
    borderRadius: ICON_SIZE / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
