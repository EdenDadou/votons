import { useTheme } from '@/Hooks'
import React from 'react'
import { Image, View, StyleSheet } from 'react-native'

export function Header() {
  const { Gutters, Layout } = useTheme()

  return (
    <View
      style={[
        styles.header,
        Layout.rowHCenter,
        Gutters.smallHPadding,
        Layout.center,
      ]}
    >
      <Image
        style={styles.logo}
        resizeMode="contain"
        source={require('@/Assets/Images/logoApp/2.png')}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  header: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },
  logo: { height: 35, width: 200 },
})
