export interface Icandidats {
  poste: any
  diplome: IDiplome[]
  network: INetwork
  experience: any
  shortName: string
  fullName: string
  age: string
  dateOfBirth: string
  partis: IPartis
  picture: any
  color: string
}

interface IPartis {
  name: string
  img: any
  history: IHistory[]
}

interface IHistory {
  date: string
  name: string
}
interface IDiplome {
  date: string
  titre: string
}

interface INetwork {
  actu: string
  fb: string
  insta: string
  twitter: string
  site: string
  prog: string
  twitterURL: string
}
export interface IPropsCandidats {
  item: Icandidats
  route: any
}

export interface IPropsItem {
  item: Icandidats
}
export interface IPropsInfo {
  item: Icandidats
  infoOpen: number
  handleInfoOpen: Function
  setDataSourceCords: Function
  dataSourceCords: any
}
export interface IExperience {
  date: string
  titre: string
}
